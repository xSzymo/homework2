package com.homework.service;

import com.homework.model.internal.Category;
import com.homework.model.internal.Item;
import com.homework.model.internal.User;
import com.homework.model.rest.PotentialBid;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class AuctionServiceTest {

    public static final String USERNAME = "Name";
    public static final String ITEMS_NAME = "Name";
    public static final String CATEGORY_NAME = "Name";
    private static final String DESCRIPTION = "Description";
    private static final String CATEGORY_DESCRIPTION = "Description";
    private AuctionService objectUnderTest;
    private ItemService itemService;
    private UserService userService;

    private User user;
    private Item item;

    @Before
    public void setUp() {
        itemService = ItemService.getInstance();
        userService = UserService.getInstance();
        objectUnderTest = new AuctionService(itemService, userService);

        user = new User();
        user.setName(USERNAME);
        user.setBids(new HashSet<>());
        userService.save(user);

        item = new Item();
        item.setName(ITEMS_NAME);
        item.setDescription(DESCRIPTION);
        item.setCategory(new Category(CATEGORY_NAME, CATEGORY_DESCRIPTION));
        item.setBids(new HashSet<>());

        itemService.save(item);
    }

    @After
    public void cleanUp() {
        itemService.findAll().forEach(itemService::delete);
        userService.findAll().stream().map(User::getName).iterator().forEachRemaining(userService::delete);
    }

    @Test
    public void recordNewBid_correct_path() {
        PotentialBid potentialBid = new PotentialBid();
        potentialBid.setItem(item);
        potentialBid.setUsername(user.getName());
        potentialBid.setPrice(new BigDecimal(100));

        objectUnderTest.recordNewBid(potentialBid);

        assertEquals(1, user.getBids().size());
        assertEquals(1, item.getBids().size());
    }

    @Test
    public void recordNewBid_wrong_username() {
        PotentialBid potentialBid = new PotentialBid();
        potentialBid.setItem(item);
        potentialBid.setUsername(user.getName() + "Non existing name");
        potentialBid.setPrice(new BigDecimal(100));

        objectUnderTest.recordNewBid(potentialBid);

        assertEquals(0, user.getBids().size());
        assertEquals(0, item.getBids().size());
    }
}