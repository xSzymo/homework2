package com.homework.service;

import com.blade.mvc.RouteContext;
import com.blade.mvc.http.HttpResponse;
import com.homework.model.internal.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ActionAPITest {
    public ActionAPI objectUnderTest;
    private Item item;

    @Spy
    private UserService userService;
    @Spy
    private ItemService itemService;
    @Spy
    private RouteContext ctx;
    @Spy
    private HttpResponse response;

    private AuctionService auctionService;
    private AuctionService spyAuctionService;

    private static String NAME = "Name";

    private static String MOCK_DATA =
            "{" +
                    "\"username\": \"Janyce\"," +
                    "\"price\": 100," +
                    "\"item\": {" +
                    "\"name\": \"Ring of Bright Water\"," +
                    "\"description\": \"You're a wizard, Harry.\"," +
                    "\"category\": {" +
                    "\"name\": \"Narrative nonfiction\"," +
                    "\"description\": \"Things are not always as they seemed, much that may seem evil can be good.\"" +
                    "}" +
                    "}" +
                    "}";
    private static String MOCK_DATA1 =
            "{\"item\": {" +
                    "\"name\": \"Ring\"," +
                    "\"description\": \"Harry.\"," +
                    "\"category\": {" +
                    "\"name\": \"Narrative\"," +
                    "\"description\": \"Non\"" +
                    "}" +
                    "}}";

    @Before
    public void setUp() {
        item = new Item("Ring", "Harry", new Category("Narrative", "Non"), Set.of(new Bid(new BigDecimal(10), 1)));
        auctionService = new AuctionService(itemService, userService);
        spyAuctionService = Mockito.spy(auctionService);

        objectUnderTest = new ActionAPI(spyAuctionService, userService, itemService);
    }

    @Test
    public void recordBid() {
        doReturn(MOCK_DATA).when(ctx).bodyToString();
        doReturn(response).when(ctx).response();
        doReturn(false).when(spyAuctionService).recordNewBid(any());

        objectUnderTest.recordBid(ctx);

        verify(spyAuctionService).recordNewBid(any());
        verify(response).badRequest();
    }

    @Test
    public void getUsers() {
        doReturn(response).when(ctx).response();
        doNothing().when(response).json(any());

        objectUnderTest.getUsers(ctx);

        verify(userService).findAll();
        verify(response).json("{\"users\":[]}");
    }

    @Test
    public void getItems() {
        doReturn(response).when(ctx).response();
        doNothing().when(response).json(any());

        objectUnderTest.getItems(ctx);

        verify(itemService).findAll();
        verify(response).json("{\"items\":[]}");
    }

    @Test
    public void getWinnerBidForItem() {
        Optional<Item> item = Optional.of(this.item);
        doReturn(MOCK_DATA1).when(ctx).bodyToString();
        doReturn(response).when(ctx).response();
        doReturn(item).when(itemService).find(any());
        doNothing().when(response).json(any());

        objectUnderTest.getWinnerBidForItem(ctx);

        verify(itemService).find(any());
        verify(response).json("{\"id\":1,\"price\":\"10.00\"}");
    }

    @Test
    public void getItemsBids() {
        Optional<Item> item = Optional.of(this.item);
        doReturn(MOCK_DATA1).when(ctx).bodyToString();
        doReturn(response).when(ctx).response();
        doReturn(item).when(itemService).find(any());
        doNothing().when(response).json(any());

        objectUnderTest.getItemsBids(ctx);

        verify(itemService).find(any());
        verify(response).json("{\"bids\":[{\"id\":1,\"price\":\"10.00\"}]}");
    }

    @Test
    public void getUsersBids() {
        doReturn(NAME).when(ctx).pathString(any());
        doReturn(Optional.of(new User(NAME, Set.of(new UserBid(item, 1))))).when(userService).find(NAME);
        doReturn(response).when(ctx).response();
        doNothing().when(response).json(any());

        objectUnderTest.getUsersBids(ctx);

        verify(userService).find(any());
        verify(response).json("{\"bids\":[{\"id\":1,\"price\":\"10.00\"}]}");
    }
}