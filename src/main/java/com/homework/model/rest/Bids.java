package com.homework.model.rest;

import com.homework.model.internal.Bid;
import lombok.Data;

import java.util.Collection;

@Data
public class Bids {
    private Collection<Bid> bids;


    public Bids(Collection<Bid> bids) {
        this.bids = bids;
    }
}
