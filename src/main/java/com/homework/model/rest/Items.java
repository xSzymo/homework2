package com.homework.model.rest;

import com.homework.model.internal.Item;
import lombok.Data;

import java.util.Set;

@Data
public class Items {
    private Set<Item> items;

    public Items(Set<Item> items) {
        this.items = items;
    }
}
