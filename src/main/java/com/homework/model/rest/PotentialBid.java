package com.homework.model.rest;

import com.homework.model.internal.Item;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class PotentialBid {
    private String username;
    private BigDecimal price;
    private Item item;
}
