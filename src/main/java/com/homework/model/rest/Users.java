package com.homework.model.rest;

import com.homework.model.internal.User;
import lombok.Data;

import java.util.Set;

@Data
public class Users {
    private Set<User> users;

    public Users(Set<User> users) {
        this.users = users;
    }
}
