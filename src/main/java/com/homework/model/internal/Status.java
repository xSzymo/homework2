package com.homework.model.internal;

public enum Status {
    SOLD("sold"), OPEN("open"), CLOSED("closed"), DELETED("deleted"), MOVED("moved"), RENAMED("renamed");

    private String name;

    Status(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
