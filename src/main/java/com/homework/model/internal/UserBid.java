package com.homework.model.internal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserBid {
    private Item item;
    private long bidId;

    public Optional<Bid> getBid() {
        return item.getBids()
                .stream()
                .filter(bid -> bid.getId() == bidId)
                .findFirst();
    }
}
