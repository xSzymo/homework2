package com.homework.model.internal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Item {
    private String name;
    private String description;
    //    private Status status;
    private Category category;
    private Set<Bid> bids;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return name.equals(item.name) &&
                Objects.equals(description, item.description) &&
                category.equals(item.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, category);
    }
}
