package com.homework.model.internal;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@Data
public class Bid {
    private long id;
    private BigDecimal price;

    public Bid(BigDecimal price, int id) {
        this.price = price;
        this.id = id;
    }
}
