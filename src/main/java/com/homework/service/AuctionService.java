package com.homework.service;

import com.homework.model.internal.Bid;
import com.homework.model.internal.Item;
import com.homework.model.internal.User;
import com.homework.model.internal.UserBid;
import com.homework.model.rest.PotentialBid;

import java.util.Optional;

public class AuctionService {
    private ItemService itemService;
    private UserService userService;

    public AuctionService(ItemService itemService, UserService userService) {
        this.itemService = itemService;
        this.userService = userService;
    }

    public boolean recordNewBid(PotentialBid potentialBid) {
        Optional<User> foundUser = userService.find(potentialBid.getUsername());
        Optional<Item> foundItem = itemService.find(item -> item.equals(potentialBid.getItem()));
        if (foundUser.isEmpty() || foundItem.isEmpty())
            return false;

        User user = foundUser.get();
        Item item = foundItem.get();
        Bid bid = new Bid(potentialBid.getPrice(), itemService.currentSize());
        UserBid userBid = new UserBid(item, bid.getId());

        user.addBid(userBid);
        userService.save(user);
        item.getBids().add(bid);
        itemService.save(item);

        return true;
    }
}
