package com.homework.service;

import com.homework.model.internal.User;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class UserService {
    private static UserService userService;
    private Set<User> users;

    private UserService() {
        users = new HashSet<>();
    }

    public static UserService getInstance() {
        if (userService == null)
            userService = new UserService();

        return userService;
    }

    public Optional<User> find(String name) {
        return users.stream()
                .filter(user -> user.getName().equals(name))
                .findFirst();
    }

    public Set<User> findAll() {
        return users;
    }

    public boolean save(User user) {
        return users.add(user);
    }

    public void update(User user, String name) {
        Optional<User> userToUpdate = find(name);
        if (userToUpdate.isPresent()) {
            users.remove(userToUpdate.get());
            users.add(user);
        }
    }

    public void delete(String name) {
        users.remove(new User(name));
    }
}
