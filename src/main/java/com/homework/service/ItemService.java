package com.homework.service;

import com.homework.model.internal.Item;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

public class ItemService {
    private static ItemService itemService;
    private Set<Item> items;

    private ItemService() {
        items = new HashSet<>();
    }

    public static ItemService getInstance() {
        if (itemService == null)
            itemService = new ItemService();

        return itemService;
    }

    public Optional<Item> find(Predicate<Item> customFilter) {
        return items.stream()
                .filter(customFilter)
                .findFirst();
    }

    public Set<Item> findAll() {
        return items;
    }

    public boolean save(Item item) {
        return items.add(item);
    }

    public void update(Item item) {
        Optional<Item> itemToUpdate = find(it -> it.equals(item));
        if (itemToUpdate.isPresent()) {
            items.remove(itemToUpdate.get());
            items.add(item);
        }
    }

    public void delete(Item item) {
        items.remove(item);
    }

    public int currentSize() {
        return items.size();
    }
}
