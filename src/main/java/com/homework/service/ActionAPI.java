package com.homework.service;

import com.blade.mvc.RouteContext;
import com.google.gson.Gson;
import com.homework.model.internal.Bid;
import com.homework.model.internal.Item;
import com.homework.model.internal.User;
import com.homework.model.internal.UserBid;
import com.homework.model.rest.Bids;
import com.homework.model.rest.Items;
import com.homework.model.rest.PotentialBid;
import com.homework.model.rest.Users;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ActionAPI {
    private final AuctionService auctionService;
    private final UserService userService;
    private final ItemService itemService;
    private Gson gson;

    public ActionAPI(AuctionService auctionService, UserService userService, ItemService itemService) {
        this.auctionService = auctionService;
        this.userService = userService;
        this.itemService = itemService;
        gson = new Gson();
    }

    public void recordBid(RouteContext ctx) {
        PotentialBid potentialBid = gson.fromJson(ctx.bodyToString(), PotentialBid.class);
        if (auctionService.recordNewBid(potentialBid))
            ctx.response().status(200);
        else
            ctx.response().badRequest();
    }


    public void getUsers(RouteContext ctx) {
        ctx.response().json(new Users(userService.findAll()));
    }

    public void getItems(RouteContext ctx) {
        ctx.response().json(new Items(itemService.findAll()));
    }

    public void getWinnerBidForItem(RouteContext ctx) {
        Item item = gson.fromJson(ctx.bodyToString(), Item.class);
        Optional<Item> foundItem = itemService.find(item::equals);

        if (foundItem.isEmpty()) {
            ctx.response().status(404);
        } else {
            Bid bid = foundItem.get()
                    .getBids()
                    .stream()
                    .max(Comparator.comparing(Bid::getPrice))
                    .get();

            ctx.response().json(bid);
        }
    }

    public void getItemsBids(RouteContext ctx) {
        Item item = gson.fromJson(ctx.bodyToString(), Item.class);
        Optional<Item> foundItem = itemService.find(item::equals);

        if (foundItem.isEmpty())
            ctx.response().status(404);
        else
            ctx.response().json(new Bids(foundItem.get().getBids()));
    }

    public void getUsersBids(RouteContext ctx) {
        Optional<User> user = userService.find(ctx.pathString(":name"));

        if (user.isEmpty()) {
            ctx.response().status(404);
        } else {
            List<Bid> bids = user.get()
                    .getBids()
                    .stream()
                    .map(UserBid::getBid)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toList());

            ctx.response().json(new Bids(bids));
        }
    }
}
