package com.homework;

import com.blade.Blade;
import com.github.javafaker.Faker;
import com.homework.model.internal.Category;
import com.homework.model.internal.Item;
import com.homework.model.internal.User;
import com.homework.model.rest.PotentialBid;
import com.homework.service.ActionAPI;
import com.homework.service.AuctionService;
import com.homework.service.ItemService;
import com.homework.service.UserService;

import java.math.BigDecimal;
import java.util.HashSet;

public class Main {
    /**
     * Record a user's bid on an item
     * Get the current winning bid for an item
     * Get all the bids for an item
     * Get all the items on which a user has bid
     */
    public static void main(String... args) {
        ItemService itemService = ItemService.getInstance();
        UserService userService = UserService.getInstance();
        AuctionService auctionService = new AuctionService(itemService, userService);
        ActionAPI api = new ActionAPI(auctionService, userService, itemService);

        loadSampleData(auctionService, itemService, userService);

        Blade.of()
                .post("/record", api::recordBid)
                .get("/users", api::getUsers)
                .get("/items", api::getItems)
                .post("/items/current/winner", api::getWinnerBidForItem)
                .post("/items/bids", api::getItemsBids)
                .get("/users/bids/:name", api::getUsersBids)
                .listen(8080)
                .start();
    }

    private static void loadSampleData(AuctionService auctionService, ItemService itemService, UserService userService) {
        Faker faker = new Faker();
        int size = 10;

        for (int i = 0; i < size; i++) {
            User user = new User();
            user.setName(faker.name().firstName());
            user.setBids(new HashSet<>());
            userService.save(user);
        }

        for (int i = 0; i < size; i++) {
            Item item = new Item();
            item.setName(faker.book().title());
            item.setDescription(faker.harryPotter().quote());
            item.setCategory(new Category(faker.book().genre(), faker.gameOfThrones().quote()));
            item.setBids(new HashSet<>());

            itemService.save(item);

            User user = userService.findAll().stream().skip(faker.number().numberBetween(0, size - 1)).findAny().get();
            PotentialBid potentialBid = new PotentialBid();
            potentialBid.setItem(item);
            potentialBid.setUsername(user.getName());
            potentialBid.setPrice(new BigDecimal(faker.number().randomNumber()));

            auctionService.recordNewBid(potentialBid);
        }
    }
}
