### How to run

Install jdk12 and maven 3.6

-----------------------------------------------------------------------------------------------------------------------
```
git clone https://gitlab.com/xSzymo/homework2.git
mvn clean install
mvn exec:java
```
Default access on ``` http://localhost:8080/ ```<br>


### Endpoints

-----------------------------------------------------------------------------------------------------------------------
- host:8080/record - POST
    - Example request to record new bid on an item - username, price and item which gets bid : 
```
    localhost:8080/record
    {
        "username": "Alyson",
        "price": 100,
        "item": {
            "name": "Harry Potter",
            "description": "Sample description.",
            "category": {
                "name": "Narrative nonfiction",
                "description": "Narrative nonfiction"
            }
        }
    }
```
        
- host:8080/users - GET
    - Example request to get all users: 
```
    localhost:8080/users
    {
    
    }
```
- host:8080/items - GET
    - Example request to get all items : 
```
    localhost:8080/items
    {
    }
```
- host:8080/items/current/winner - POST
    - Example request to get current winning bid of the item : 
```
    localhost:8080/items/current/winner
    {
        "name": "Harry Potter",
        "description": "Sample description.",
        "category": {
            "name": "Narrative nonfiction",
            "description": "Narrative nonfiction"
        }
    }
```
- host:8080/items/bids - POST
    - Example request to get bids of the item: 
```
    localhost:8080/items/bids
    {
        "name": "Harry Potter",
        "description": "Sample description.",
        "category": {
            "name": "Narrative nonfiction",
            "description": "Narrative nonfiction"
        }
    }
```
- host:8080/users/bids/:name - GET
    - Example request to get bids of the user - where name stands for username: 
```
    localhost:8080/users/bids/:name
    {
    }
```

### Frameworks :
- Blade
- Lombok
- JUnit
- Mockito
- JavaFaker
  <br><br>
